public class SymbolTableLocalTempVariableItem extends SymbolTableVariableItemBase {
	public SymbolTableLocalTempVariableItem(Variable variable, int offset) {
		super(variable, offset);
	}

	public Register getBaseRegister() {
		return Register.SP;
	}

	@Override
	public boolean useMustBeComesAfterDef() {
		return false;
	}
}