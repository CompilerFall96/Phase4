
public class Pass2Utils {

    public static int scopeDepth = 0;
    public static Stack<Integer> foreachScopeDepth = new Stack<>();

    public static void print(String message, int line) {
        Utils.print(line + ") " + message);
    }

    public static void beginScope() {
        scopeDepth++;
        int offset = 0;
        if(SymbolTable.top != null)
            offset = SymbolTable.top.getOffset(Register.TEMP0);

        SymbolTable.push();

        SymbolTable.top.setOffset(Register.TEMP0, offset);

        Translator.getInstance().pushStackPointer(SymbolTable.top.getOffset(Register.SP));
    }

    public static void endScope() {
        scopeDepth--;
        Utils.print("Stack offset: " + SymbolTable.top.getOffset(Register.SP) + ", Global offset: " + SymbolTable.top.getOffset(Register.GP)
                + ", TEMP0 offset: " + SymbolTable.top.getOffset(Register.TEMP0));
        SymbolTable.pop();
        Translator.getInstance().popStackPointer();
    }

    public static boolean isIntSubType(Type type) {
        return (type instanceof IntType || type instanceof NoType);
    }

    public static Type getOperatorReturnType(Type type, int line, String operator) {
        if (!Pass2Utils.isIntSubType(type)) {
            String message = "Invalid operand for type " + type.toString();
            if (!operator.equals(""))
                message += " for operator " + operator;
            Utils.printError(message, line);
            return NoType.getInstance();
        } else {
            return type;
        }
    }

    public static Type getCompareReturnType(Type type_a, Type type_b, int line) {
        if (!type_a.equals(type_b)) {
            Utils.printError(String.format("Unequal operand types for operator compare between types %s and %s",
                type_a.toString(), type_b.toString()), line);
            return NoType.getInstance();
        } else {
            return IntType.getInstance();
        }
    }

    public static Type getAssignReturnType(Type type_a, Type type_b, int line) {
        if (!Pass2Utils.checkTypesEquality(type_a, type_b, line)) {
            return NoType.getInstance();
        } else {
            return type_a;
        }
    }

    public static boolean checkTypesEquality(Type type_a, Type type_b, int line) {
        if (!type_a.equals(type_b) && !(type_b instanceof NoType)) {
            Utils.printError(String.format("Can't assign type %s to %s", type_b.toString(), type_a.toString()), line);
            return false;
        }
        return true;
    }

    public static boolean isWriteArgument(Type type) {
        boolean arrayTypeCheck = false;
        if (type instanceof ArrayType)
            arrayTypeCheck = ((ArrayType)type).getType() instanceof CharType;
        return (type instanceof IntType || type instanceof NoType || type instanceof CharType || arrayTypeCheck);
    }

    public static void updateForeachVar(String name, Type type, int line) {
        putForeachVar(name, type);
        print(String.format("Foreach variable '%s' with type %s defined.", name, type.toString()), line);
    }

    public static void putForeachVar(String name, Type type){
        SymbolTable.top.update(
            new SymbolTableForeachLocalVariableItem(
                new Variable(name, type),
                SymbolTable.top.getOffset(Register.TEMP0)
            )
        );
    }
}
