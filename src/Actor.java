public class Actor {
    public Actor(String name, int queueLength) {
        this.name = name;
        this.queueLength = queueLength;
    }

    public String getQueueLabel() {
        return name + "_queue";
    }

    public String getLabel() {
        return name + "_label";
    }

    public String getStartLabel() {
        return name + "_start";
    }

    public String getEndLabel() {
        return name + "_end";
    }

    public String getName() {
        return name;
    }

    public int getQueueLength() {
        return queueLength;
    }

    @Override
    public String toString() {
        return String.format("Actor %s with queueLength %d", name, queueLength);
    }

    private String name;
    private int queueLength;
}
