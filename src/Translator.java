/**
 * Created by vrasa on 12/26/2016.
 */

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Translator {

    private final String ACTIVE_RECEIVER_CNT_REG = Register.TEMP4.toString();
    private final String TOP_RECEIVER_REG = Register.TEMP5.toString();

    private File output;
    private ArrayList <String> instructions;
    private ArrayList <String> beforeSchedulerInstructions;
    private ArrayList <String> initInstructions;
    private ArrayList <String> dataSegmentInstructions;
    private Stack<String> foreachEndPointer;
    public ArrayList<Actor> actors;

    private static Translator instance = null;

    public static Translator getInstance() {
        if (instance == null)
            instance = new Translator();
        return instance;
    }

    private Translator() {
        instructions = new ArrayList<>();
        initInstructions = new ArrayList<>();
        dataSegmentInstructions = new ArrayList<>();
        beforeSchedulerInstructions = new ArrayList<>();
        foreachEndPointer = new Stack<>();
        actors = new ArrayList<>();
        output = new File("out.asm");
        try {
            output.createNewFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pushStackPointer(Integer pointer) {
        instructions.add("#begin scope");
        pushToCustomStack("sp", "t3");
    }

    public void popStackPointer() {
        instructions.add("lw $sp, 4($t3)");
        instructions.add("addi $t3, $t3, 4");
        instructions.add("#end scope");
    }

    public void pushForeachEndPointer(String endLabel) {
        instructions.add("# foreach scope begin");
        foreachEndPointer.push(endLabel);
    }

    public String popForeachEndPointer() {
        instructions.add("# foreach scope ends");
        return foreachEndPointer.pop();
    }

    public String getForeachEndPointer() {
        String topLabel = foreachEndPointer.pop();
        foreachEndPointer.push(topLabel);
        return topLabel;
    }

    private void initialRegisters(PrintWriter writer) {
        // t0 : foreach elements sp
        // t1 : foreach counters
        // t2 : foreach fp
        // t3 : scope pointers
        writer.println("move $fp, $sp");
        writer.println("move $t0, $gp");
        writer.println("addi $t0, $t0, -4");
        writer.println("addi $t1, $t0, -8000");
        writer.println("move $t2, $t0");
        writer.println("addi $t3, $t1, -1000");
        writer.println("li " + ACTIVE_RECEIVER_CNT_REG + ", 0");
    }

    public void makeOutput() {
        this.addSystemCall(10);
        try {
            PrintWriter writer = new PrintWriter(output);
            writer.println(".data");
            for (int i=0;i<dataSegmentInstructions.size();i++) {
                writer.println(dataSegmentInstructions.get(i));
            }
            writer.println(".text");
            getAllLabelCommands();
            writer.println("main:");
            initialRegisters(writer);
            for (int i=0;i<initInstructions.size();i++) {
                writer.println(initInstructions.get(i));
            }
            for (int i=0;i<beforeSchedulerInstructions.size();i++) {
                writer.println(beforeSchedulerInstructions.get(i));
            }
            writer.println("j Scheduler");
            generateSchedulerCode();
            for (int i=0;i<instructions.size();i++) {
                writer.println(instructions.get(i));
            }
            writer.close();
        } catch (Exception e) { e.printStackTrace(); }
    }

    private void getAllLabelCommands() {
        getEqualityCheck();
        getUnEqualityCheck();
        getAndLogicalOperation();
        getOrLogicalOperation();
        getNotLogicalOperation();
    }

    public void addForeachCountVariables() {
        instructions.add("# adding a foreach count variable to stack");
        instructions.add("li $a0, 0");
        pushToCustomStack("a0", "t1");
        instructions.add("# end of adding a foreach count variable to stack");
    }

    public void addForeachVariables(int x) {
        instructions.add("# adding a foreach variable to stack");
        instructions.add("li $a0, " + x);
        pushToCustomStack("a0", "t0");
        instructions.add("# end of adding a foreach variable to stack");
    }

    public void addForeachVariables(int size, int initial) {
        instructions.add("# start of adding foreach variable array to stack");
        int wordSize = IntType.getInstance().size();
        for (int i = 0; i < size/wordSize; i++) {
            instructions.add("li $a0, " + initial);
            pushToCustomStack("a0", "t0");
        }
        instructions.add("# end of adding foreach variable array to stack");
    }

    public void addForeachVariableToStack(String s, int adr) {
        instructions.add("# start of adding foreach variable to stack");
        instructions.add("lw $a0, " + adr + "($t2)");
        pushStack("a0");
        instructions.add("# end of adding foreach variable to stack");
    }

    public void addForeachVariableAddressToStack(String s, int adr) {
        instructions.add("# start of adding foreach variable address to stack");
        instructions.add("addiu $a0, $t2, " + adr);
        pushStack("a0");
        instructions.add("# end of adding foreach variable address to stack");
    }

    public void generateForeachCode(ArrayType type, String endLabel) {
        instructions.add("# start of foreach iteration");
        // check loop count
        instructions.add("lw $a0, 4($t1)");
        instructions.add("li $a1, " + type.getExactSize());
        instructions.add("bge $a0, $a1, " + endLabel);

        // update foreach element
        instructions.add("addi $a1, $sp, " + (type.size() - type.getType().size()));
        instructions.add("li $a2, " + type.getType().size());
        instructions.add("mul $a0, $a0, $a2");
        instructions.add("sub $a1, $a1, $a0");

        instructions.add("move $a0, $t0");
        // a1 is address of array item in sp
        // a0 is address of array foreach item in t0
        for (int i = 0; i < type.getType().size() / 4 ; i++) {
            instructions.add("lw $a2, 4($a1)");
            instructions.add("sw $a2, 4($a0)");
            instructions.add("addi $a0, $a0, 4");
            instructions.add("addi $a1, $a1, 4");
        }

        // updating foreach count variable
        instructions.add("lw $a0, 4($t1)");
        instructions.add("addi $a0, $a0, 1");
        instructions.add("sw $a0, 4($t1)");

        instructions.add("# end of foreach iteration");
    }

    public void generateForeachEndCode(ArrayType type) {
        putLabel(popForeachEndPointer());
        instructions.add("addiu $t1, $t1, 4");
        instructions.add("addiu $t0, $t0, " + type.getType().size());
        instructions.add("addiu $sp, $sp, " + type.size());
    }

    public void addToStack(int x) {
        instructions.add("# adding a number to stack");
        instructions.add("li $a0, " + x);
        pushStack("a0");
        instructions.add("# end of adding a number to stack");

    }

    public void addToStack(String s, int adr) {
//        int adr = table.getAddress(s)*(-1);
        instructions.add("# start of adding variable to stack");
        instructions.add("lw $a0, " + adr + "($fp)");
        pushStack("a0");
        instructions.add("# end of adding variable to stack");
    }

    public void addToStack(int size, int initial) {
        instructions.add("# start of adding variable array to stack");
        int wordSize = IntType.getInstance().size();
        for (int i = 0; i < size/wordSize; i++) {
            instructions.add("li $a0, " + initial);
            pushStack("a0");
        }
        instructions.add("# end of adding variable array to stack");
    }

    public void addAddressToStack(String s, int adr) {
//        int adr = table.getAddress(s)*(-1);
        instructions.add("# start of adding address to stack");
        instructions.add("addiu $a0, $fp, " + adr);
        pushStack("a0");
        instructions.add("# end of adding address to stack");
    }

    public void addGlobalAddressToStack(String s, int adr) {
//        int adr = table.getAddress(s)*(-1);
        instructions.add("# start of adding global address to stack");
        instructions.add("addiu $a0, $gp, " + adr);
        pushStack("a0");
        instructions.add("# end of adding global address to stack");
    }

    public void popStack() {
        instructions.add("# pop stack");
        instructions.add("addiu $sp, $sp, 4");
        instructions.add("# end of pop stack");
    }

    public void popStack(int size) {
        instructions.add("# pop multiple stack");
        instructions.add("addiu $sp, $sp, " + size);
        instructions.add("# end of pop multiple stack");
    }

    public void pushStack(String register) {
        instructions.add("# push stack");
        instructions.add("sw $" + register + ", 0($sp)");
        instructions.add("addiu $sp, $sp, -4");
        instructions.add("# end of push stack");
    }

    public void pushToCustomStack(String register, String baseRegister) {
        instructions.add("# push " + baseRegister + " stack");
        instructions.add("sw $" + register + ", 0($" + baseRegister + ")");
        instructions.add("addiu $" + baseRegister + ", $" + baseRegister + ", -4");
        instructions.add("# end of push " + baseRegister + " stack");
    }

    public void addSystemCall(int x) {
        instructions.add("# start syscall " + x);
        instructions.add("li $v0, " + x);
        instructions.add("syscall");
        instructions.add("# end syscall");
    }

    public void assignCommand(boolean isInitial) {
        instructions.add("# start of assign");
        instructions.add("lw $a0, 4($sp)");
        popStack();
        instructions.add("lw $a1, 4($sp)");
        popStack();
        instructions.add("sw $a0, 0($a1)");
        if (!isInitial)
            pushStack("a0");
        instructions.add("# end of assign");
    }

    public void assignCommand(ArrayType array, boolean isInitial) {
        instructions.add("# start of array assign");
        instructions.add("addi $a0, $sp, " + array.size());
        instructions.add("lw $a0, 4($a0)");
        instructions.add("addi $a0, $a0, -" + (array.size()-4));
        for (int i = 0; i < array.size() / 4 ; i++) {
            instructions.add("lw $a1, 4($sp)");
            instructions.add("sw $a1, 0($a0)");
            instructions.add("addi $a0, $a0, 4");
            popStack();
        }
        instructions.add("lw $a0, 4($sp)");
        popStack();
        if (!isInitial) {
            for (int i = 0; i < array.size() / 4; i++) {
                instructions.add("lw $a1, 0($a0)");
                instructions.add("addi $a0, $a0, -4");
                pushStack("a1");
            }
        }

        instructions.add("# end of array assign");
    }

    public void getEqualityCheck() {
        instructions.add("EqualityCheck:");
        instructions.add("beq $a0, $a1, Equal");
        instructions.add("li $a0, 0");
        instructions.add("jr $ra");
        instructions.add("Equal:");
        instructions.add("li $a0, 1");
        instructions.add("jr $ra");
    }

    public void getUnEqualityCheck() {
        instructions.add("UnEqualityCheck:");
        instructions.add("bne $a0, $a1, UnEqual");
        instructions.add("li $a0, 0");
        instructions.add("jr $ra");
        instructions.add("UnEqual:");
        instructions.add("li $a0, 1");
        instructions.add("jr $ra");
    }

    public void getAndLogicalOperation() {
        instructions.add("AndLogicalOperation:");
        instructions.add("beqz $a0, AndLogical");
        instructions.add("beqz $a1, AndLogical");
        instructions.add("li $a0, 1");
        instructions.add("jr $ra");
        instructions.add("AndLogical:");
        instructions.add("li $a0, 0");
        instructions.add("jr $ra");
    }

    public void getOrLogicalOperation() {
        instructions.add("OrLogicalOperation:");
        instructions.add("bnez $a0, OrLogical");
        instructions.add("bnez $a1, OrLogical");
        instructions.add("li $a0, 0");
        instructions.add("jr $ra");
        instructions.add("OrLogical:");
        instructions.add("li $a0, 1");
        instructions.add("jr $ra");
    }

    public void getNotLogicalOperation() {
        instructions.add("NotLogicalOperation:");
        instructions.add("beqz $a0, NotLogical");
        instructions.add("li $a0, 0");
        instructions.add("jr $ra");
        instructions.add("NotLogical:");
        instructions.add("li $a0, 1");
        instructions.add("jr $ra");
    }

    public void twoOperandOperationCommand(String s) {
        instructions.add("# operation " + s);
        instructions.add("lw $a0, 4($sp)");
        popStack();
        instructions.add("lw $a1, 4($sp)");
        popStack();
        if (s.equals("*"))
            instructions.add("mul $a0, $a0, $a1");
        else if (s.equals("/"))
            instructions.add("div $a0, $a1, $a0");
        else if (s.equals("+"))
            instructions.add("add $a0, $a0, $a1");
        else if (s.equals("-"))
            instructions.add("sub $a0, $a1, $a0");
        else if (s.equals(">"))
            instructions.add("slt $a0, $a0, $a1");
        else if (s.equals("<"))
            instructions.add("slt $a0, $a1, $a0");
        else if (s.equals("=="))
            instructions.add("jal EqualityCheck");
        else if (s.equals("<>"))
            instructions.add("jal UnEqualityCheck");
        else if (s.equals("and"))
            instructions.add("jal AndLogicalOperation");
        else if (s.equals("or"))
            instructions.add("jal OrLogicalOperation");

        pushStack("a0");
        instructions.add("# end of operation " + s);
    }

    public void arrayEqualCommand(String operation, ArrayType array) {
        instructions.add("# array equal check");
        instructions.add("move $k0, $sp");
        instructions.add("addi $k1, $sp, -" + array.size());
        instructions.add("li $a2, 1");
        for (int i = 0; i < array.size()/4; i++) {
            instructions.add("lw $a0, 4($k0)");
            instructions.add("lw $a1, 4($k1)");
            instructions.add("jal EqualityCheck");
            instructions.add("and $a2, $a2, $a0");
            instructions.add("addi $k0, $k0, 4");
            instructions.add("addi $k1, $k1, 4");
        }
        instructions.add("move $a0, $a2");
        if (operation.equals("=="))
            instructions.add("jal NotLogicalOperation");

        pushStack("a0");
        instructions.add("#end of array equal check");
    }

    public void oneOperandOperationCommand(String s) {
        instructions.add("# operation " + s);
        instructions.add("lw $a0, 4($sp)");
        popStack();
        if (s.equals("not"))
            instructions.add("jal NotLogicalOperation");
        else if (s.equals("-"))
            instructions.add("neg $a0, $a0");
        pushStack("a0");
        instructions.add("# end of operation " + s);
    }

    public void read(int size) {
        instructions.add("# start reading ");
        for(int i = 0; i < size; i++) {
            addSystemCall(12);
            pushStack("v0");
        }
        instructions.add("addi $a0, $zero, 10");
        this.addSystemCall(11);
        instructions.add("# end reading ");
    }

    public void writeInt() {
        instructions.add("# writing integer");
        instructions.add("lw $a0, 4($sp)");
        this.addSystemCall(1);
        popStack();
        instructions.add("addi $a0, $zero, 10");
        this.addSystemCall(11);
        instructions.add("# end of writing integer");
    }

    public void writeChar() {
        instructions.add("# writing char");
        instructions.add("lw $a0, 4($sp)");
        this.addSystemCall(11);
        popStack();
        instructions.add("addi $a0, $zero, 10");
        this.addSystemCall(11);
        instructions.add("# end of writing char");
    }

    public void writeString(int size) {
        instructions.add("# writing string");
        instructions.add("addi $a1, $sp, " + size);
        for (int i=0; i < size/4; i++) {
            instructions.add("lw $a0, 0($a1)");
            this.addSystemCall(11);
            instructions.add("addi $a1, $a1, -4");
        }
        for (int i=0; i < size/4; i++) {
            popStack();
        }
        instructions.add("addi $a0, $zero, 10");
        this.addSystemCall(11);
        instructions.add("# end of writing string");
    }

    public void addGlobalToStack(int adr) {
//        int adr = table.getAddress(s)*(-1);
        instructions.add("# start of adding global variable to stack");
        instructions.add("lw $a0, " + adr + "($gp)");
        pushStack("a0");
        instructions.add("# end of adding global variable to stack");
    }

    public void addGlobalVariable(int adr, int x) {
//        int adr = table.getAddress(s)*(-1);
        initInstructions.add("# adding a global variable");
        initInstructions.add("li $a0, " + x);
        initInstructions.add("sw $a0, " + adr + "($gp)");
        initInstructions.add("# end of adding a global variable");
    }

    public void addGlobalVariable(int offset, int size, int initial) {
        initInstructions.add("# adding a global array variable");
        int wordSize = IntType.getInstance().size();
        for (int i = 0; i < size/wordSize; i++) {
            initInstructions.add("li $a0, " + initial);
            initInstructions.add("sw $a0, " + offset + "($gp)");
            offset += wordSize;
        }
        initInstructions.add("# end of adding a global array variable");
    }

    public void addActorQueue(String name, int size) {
        dataSegmentInstructions.add(name + "_queue: .space " + size*4);
        dataSegmentInstructions.add(name + "_start: .word 0");
        dataSegmentInstructions.add(name + "_end: .word 0");
    }

    public void addConditionalJump(String label) {
        instructions.add("# start if condition");
        instructions.add("lw $a0, 4($sp)");
        popStack();
        instructions.add("beqz $a0, " + label);
        instructions.add("# end if condition");
    }

    public void addJump(String label) {
        instructions.add("# start if jump");
        instructions.add("j " + label);
        instructions.add("# end if jump");
    }

    public void putLabel(String label) {
        instructions.add(label + ":");
    }

    public void addArrayAddressToStack(int count, ArrayType type) {
        instructions.add("# start pushing array address to stack");
        calcArrayOffset(count, type);
        instructions.add("sub $a0, $a1, $a0");
        pushStack("a0");
        instructions.add("# end pushing array address to stack");
    }

    public void addArrayToStack(int count, ArrayType type) {
        instructions.add("# start pushing array to stack");
        calcArrayOffset(count, type);
        instructions.add("sub $a0, $a1, $a0");
        for (int i = 0; i < type.getChildType(count).size() / 4; i++) {
            instructions.add("lw $a1, 0($a0)");
            pushStack("a1");
            instructions.add("addi $a0, $a0, -4");
        }
        instructions.add("# end pushing array to stack");
    }

    private void calcArrayOffset(int count, ArrayType type) {
        // calc offset
        instructions.add("li $a0, 0");
        for (int i = 1; i <= count; i++) {
            instructions.add("lw $a1, 4($sp)");
            popStack();
            instructions.add("mul $a1, $a1, " + type.getChildType(count - i + 1).size());
            instructions.add("add $a0, $a0, $a1");
        }
        // getting array element address
        instructions.add("lw $a1, 4($sp)");
        popStack();
    }

    public void addBreakCode() {
        addJump(getForeachEndPointer());
        // updating stack pointers
        int foreachScopeDepth = Pass2Utils.foreachScopeDepth.pop();
        int scopeDepth = Pass2Utils.scopeDepth;
        for (int i = foreachScopeDepth; i <= scopeDepth; i++)
            popStackPointer();
    }


    public void addReceiverInitialCode(Receiver receiver) {
        instructions.add("# start receiver");
        putLabel(LabelGenerator.getReceiverLabel(receiver));
    }

    public void addReceiverEndCode(Actor actor) {
        removeReceiverFromActorQueue(actor);
        instructions.add("jr $ra");
        instructions.add("# end receiver");
    }

    public void addReceiverToActorQueue(Actor actor, Receiver receiver) {
        // TODO: adding arguments for receivers
        instructions.add("# adding receiver to actor queue");
        String labelName = actor.getName() + "_" + receiver.getName();

        instructions.add("lw $a0, " + actor.getEndLabel());

        instructions.add("la $a1, " + actor.getQueueLabel());
        instructions.add("add $a1, $a1, $a0");
        instructions.add("la $a0, " + labelName);
        instructions.add("sw $a0, 0($a1)");

        updateActorLabel(actor, "end");

        instructions.add("addi " + ACTIVE_RECEIVER_CNT_REG + ", " + ACTIVE_RECEIVER_CNT_REG + ", 1");
        instructions.add("# end adding receiver to actor queue");
    }

    public void addInitReceiverToActorQueue(Actor actor) {
        beforeSchedulerInstructions.add("# adding init receiver to actor queue");
        String labelName = actor.getName() + "_init" ;

        beforeSchedulerInstructions.add("la $a0, " + labelName);
        beforeSchedulerInstructions.add("sw $a0, " + actor.getQueueLabel());

        beforeSchedulerInstructions.add("li $a0, 4");
        beforeSchedulerInstructions.add("sw $a0, " + actor.getEndLabel());

        beforeSchedulerInstructions.add("addi " + ACTIVE_RECEIVER_CNT_REG + ", " + ACTIVE_RECEIVER_CNT_REG + ", 1");
        beforeSchedulerInstructions.add("# end adding init receiver to actor queue");
    }

    private void updateActorLabel(Actor actor, String label) {
        instructions.add("# updating actor " + label + " label");
        String finalLabel = actor.getStartLabel();
        if (label.equals("end"))
            finalLabel = actor.getEndLabel();

        instructions.add("lw $a1," + finalLabel);
        instructions.add("addi $a3, $a1, 4");
        instructions.add("li $a2, " + actor.getQueueLength()*4);
        instructions.add("div $a3, $a2");
        instructions.add("mfhi $a3");
        instructions.add("sw $a3," + finalLabel);
        instructions.add("lw $a1," + finalLabel);

        instructions.add("# end updating actor " + label + " label");
    }

    public void removeReceiverFromActorQueue(Actor actor) {
        instructions.add("# removing receiver from actor queue");
        instructions.add("la $a0, " + actor.getQueueLabel());
        instructions.add("lw $a1, " + actor.getStartLabel());
        instructions.add("add $a0, $a0, $a1");

        instructions.add("lw " + TOP_RECEIVER_REG + ", 0($a0)");
        updateActorLabel(actor, "start");
        instructions.add("addiu " + ACTIVE_RECEIVER_CNT_REG + ", " + ACTIVE_RECEIVER_CNT_REG + ", -1");
        instructions.add("# end removing receiver from actor queue");
    }

    private void generateSchedulerCode() {
        instructions.add("# start scheduler");
        putLabel("Scheduler");
        instructions.add("blez " + ACTIVE_RECEIVER_CNT_REG + ", END");
        for (Actor actor: actors) {
            instructions.add("lw $a0, " + actor.getStartLabel());
            instructions.add("lw $a1, " + actor.getEndLabel());
            instructions.add("beq $a0, $a1, " + actor.getLabel());

            instructions.add("la $a0, " + actor.getQueueLabel());
            instructions.add("lw $a1, " + actor.getStartLabel());
            instructions.add("add $a0, $a0, $a1");
            instructions.add("lw " + TOP_RECEIVER_REG + ", 0($a0)");

            instructions.add("jal " + TOP_RECEIVER_REG);

            putLabel(actor.getLabel());
        }

        addJump("Scheduler");
        putLabel("END");
        addSystemCall(10);
        instructions.add("# end scheduler");
    }
}