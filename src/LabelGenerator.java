public class LabelGenerator {
    static final String LABEL_BASE = "label_";
    public static String currentActor = "";
    public static int counter = 0;

    public static String getNextLabel() {
        return LABEL_BASE + counter++;
    }
    public static String getReceiverLabel(Receiver receiver) {
        return currentActor + "_" + receiver.getName();
    }
}
