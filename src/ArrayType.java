public class ArrayType extends Type {

	private final Type type;
	private final int size;

	public ArrayType(Type type, int size) {
		this.type = type;
		this.size = size;
	}

	public Type getType() {
	    return type;
    }

    public int getExactSize() {
		return size;
	}

	public int size() {
		return type.size() * size;
	}

	@Override
	public int getDimension() {
		return type.getDimension() + 1;
	}

	@Override
	public boolean equals(Object other) {
		if(other instanceof ArrayType) {
			ArrayType arrayType = (ArrayType) other;
			if (arrayType.size != size)
				return false;
			return type.equals(arrayType.type);
		}
		return false;
	}

	public Type getBaseType() {
		if (type.getDimension() == 0)
			return type;
		return type.getBaseType();
	}

	public Type getChildType(int level) {
		if(level == 0)
			return this;
	    if(level == 1)
	        return type;
	    return ((ArrayType) type).getChildType(level - 1);
    }

	@Override
	public String getIdentifier() {

		return toString() + ":" + type.getBaseType() + ":" + getDimension();
	}

	@Override
	public String toString() {
//		return "array";
		return "array(" + size + "," + type.toString() + ")";
	}
}